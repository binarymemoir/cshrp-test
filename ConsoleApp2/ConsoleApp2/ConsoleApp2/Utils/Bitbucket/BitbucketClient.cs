﻿using AlarmBusinessObjects.Jira.Models;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;

namespace AlarmBusinessObjects.Jira
{
    public class JiraClient : RestClient
    {
        public JiraClient(string username, string password)
        {
            this.BaseUrl = new Uri("https://jira.corp.adcinternal.com/jira/rest/");
            this.AddDefaultHeader("Content-Type", "application/json");

            // Right now, we're using basic auth. This is not as secure as we'd like.
            // Should look into adding a JiraAuthenticationProvider so we can use OAuth.
            // https://developer.atlassian.com/cloud/jira/platform/jira-rest-api-oauth-authentication/
            this.Authenticator = new HttpBasicAuthenticator(username, password);

            // Need to specify custom serializer to handle unusual formats.
            JsonDeserializer deserializer = new JsonDeserializer
            {
                DateFormat = "yyyy-MM-ddTHH:mm:ss"
            };

            this.RemoveHandler("application/json");
            this.AddHandler("application/json", deserializer);

            this.RemoveHandler("text/json");
            this.AddHandler("text/json", deserializer);

            this.RemoveHandler("*");
            this.AddHandler("*", deserializer);
        }

        #region Helpers

        private IRestResponse MakeCall(IRestRequest request)
        {
            try
            {
                return this.Execute(request);
            }
            catch (Exception ex)
            {
                return new RestResponse { ResponseStatus = ResponseStatus.Aborted, ErrorException = ex, ErrorMessage = ex.Message };
            }
        }

        private IRestResponse<T> MakeCall<T>(IRestRequest request) where T : new()
        {
            try
            {
                IRestResponse<T> response = this.Execute<T>(request);
                HandleEmptyResponse(response);
                return response;
            }
            catch (Exception ex)
            {
                return new RestResponse<T> { ResponseStatus = ResponseStatus.Aborted, ErrorException = ex, ErrorMessage = ex.Message };
            }
        }

        private static void HandleEmptyResponse<T>(IRestResponse<T> response) where T : new()
        {
            // If status is OK and content is empty. Fix Serialization issue.
            if (response.StatusCode == System.Net.HttpStatusCode.OK && response.ContentLength == 0)
            {
                response.Data = new T();
                response.ErrorException = null;
                response.ErrorMessage = null;
                response.ResponseStatus = ResponseStatus.Completed;
            }
        }

        /// <summary>
        /// Makes a request where the expected response is either a <see cref="BaseModel"/> or a <see
        /// cref="List{T}"/>, but not both
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="request"></param>
        /// <returns></returns>
        private IRestResponse MakeListCall<T>(IRestRequest request, out BaseList<T> data) where T : BaseModel, new()
        {
            data = null;
            // Get generic response
            IRestResponse response = this.MakeCall(request);

            // Set content type to json if undefined (this happens on some errors)
            response.ContentType = response.ContentType ?? "application/json";

            // Deserialize and check if this is a BaseModel
            IRestResponse<BaseModel> baseModelResponse = this.Deserialize<BaseModel>(response);
            if (baseModelResponse.Data != null)
            {
                // We had a BaseModel return it
                data = new BaseList<T>(baseModelResponse.Data);
            }
            else
            {
                IRestResponse<List<T>> listResponse = this.Deserialize<List<T>>(response);
                if (listResponse.Data != null)
                {
                    // We had a List return it
                    data = new BaseList<T> { Items = listResponse.Data };
                }
            }

            return response;
        }

        /// <summary>
        /// Uses reflection to implement change added to RestSharp 106. <see cref="https://github.com/restsharp/RestSharp/blob/07a2cec8fbe5b0d1dc3df7067e0338dc79603b86/RestSharp/RestClient.cs#L252"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="response"></param>
        /// <returns></returns>
        private IRestResponse<T> Deserialize<T>(IRestResponse response)
        {
            System.Reflection.MethodInfo methodInfo = typeof(RestClient).GetMethod("Deserialize",
                             System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
            System.Reflection.MethodInfo genericMethodInfo = methodInfo.MakeGenericMethod(typeof(T));
            return genericMethodInfo.Invoke(this, new object[] { response.Request, response }) as IRestResponse<T>;
        }

        #endregion Helpers

        #region Body Models

        private class Comment
        {
            public string body { get; set; }
        }

        #endregion Body Models

        public IRestResponse<JiraIssue> GetIssue(string issueId)
        {
            IRestRequest request = new RestRequest("api/2/issue/{issueId}", Method.GET);
            request.AddUrlSegment("issueId", issueId);

            return this.MakeCall<JiraIssue>(request);
        }

        public IRestResponse<BaseModel> AddComment(string issueId, string comment)
        {
            Comment newComment = new Comment { body = comment };

            IRestRequest request = new RestRequest("api/2/issue/{issueId}/comment", Method.POST);
            request.AddUrlSegment("issueId", issueId);
            request.AddJsonBody(newComment);

            return this.MakeCall<BaseModel>(request);
        }
    }
}
