﻿using System.Collections.Generic;
using System.Linq;

namespace AlarmBusinessObjects.Jira.Models
{
    public class BaseList<T> : BaseModel
    {
        public BaseList()
        {
        }

        public BaseList(BaseModel @base) : base()
        {
        }

        public override bool HasError => this.Items.Any(e => (e as BaseModel)?.HasError ?? false);

        public List<T> Items { get; set; }
    }
}
