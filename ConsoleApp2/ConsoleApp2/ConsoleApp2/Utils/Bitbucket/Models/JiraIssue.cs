﻿namespace AlarmBusinessObjects.Jira.Models
{
    public class JiraIssue : BaseModel
    {
        public string id { get; set; }

        public string self { get; set; }

        public string key { get; set; }

        public JiraFields fields { get; set; }
    }
}
