﻿using AlarmUtils;
using System.Collections.Generic;

namespace AlarmBusinessObjects.Jira.Models
{
    public class BaseModel
    {
        public List<string> errorMessages { get; set; }

        public string errors { get; set; }

        public virtual bool HasError => !errors.IsNullOrEmpty() || (errorMessages?.Count ?? 0) > 0;

        public virtual string FormattedErrorMsg => $"Errors: {errorMessages?.JoinToString(". ") ?? string.Empty}";
    }
}
