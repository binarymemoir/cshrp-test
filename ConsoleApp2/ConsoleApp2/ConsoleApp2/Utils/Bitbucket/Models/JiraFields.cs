﻿using System;
using System.Collections.Generic;

namespace AlarmBusinessObjects.Jira.Models
{
    public class JiraFields
    {
        /*
         * We have quite a few custom fields that we can integrate when needed.
         * We'll just have to figure out what maps to what. e.g. customfield_10510 -> estimated check in
         * 
         * Example:
         * [DeserializeAs(Name = "customfield_10510")]
         * public JiraEstimatedCheckIn estimatedCheckIn { get; set; }
         */

        public List<string> labels { get; set; }

        public /*JiraIssueLinks[]*/ string issuelinks { get; set; }

        public /*JiraUser*/ string assignee { get; set; }

        public /*JiraUser*/ string reporter { get; set; }

        public DateTime updated { get; set; }

        public string description { get; set; }

        public string summary { get; set; }

        public /*JiraPriority*/ string priority { get; set; }

        public /*JiraStatus*/ string status { get; set; }
    }
}
