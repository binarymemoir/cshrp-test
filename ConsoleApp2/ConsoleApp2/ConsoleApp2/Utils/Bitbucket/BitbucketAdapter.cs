using AlarmBusinessObjects.Jira.Models;
using AlarmUtils;
using RestSharp;
using System.Linq;

namespace AlarmBusinessObjects.Jira
{
    public class JiraAdapter
    {
        private readonly JiraClient client;
        private readonly Logger log = Logger.GetInstance();

        public JiraAdapter(string username, string password)
        {
            this.client = new JiraClient(username, password);
        }

        #region Helpers

        private static bool WasSuccessful<T>(IRestResponse<T> response) where T : BaseModel
        {
            return IsSuccessful(response) && ((!response.Data?.HasError) ?? false);
        }

        private static bool WasSuccessful<T>(IRestResponse response, BaseList<T> data) where T : BaseModel
        {
            return IsSuccessful(response) && (!data?.HasError ?? false) && (data?.Items.All(baseModel => !baseModel.HasError) ?? false);
        }

        private static bool IsSuccessful(IRestResponse response)
        {
            return (int)response.StatusCode >= 200 && (int)response.StatusCode <= 299 && response.ResponseStatus == ResponseStatus.Completed;
        }

        #endregion Helpers

        // https://docs.atlassian.com/software/jira/docs/api/REST/7.6.1/#api/2/issue-getIssue
        public JiraIssue GetIssue(string issueId)
        {
            IRestResponse<JiraIssue> getIssueResponse = this.client.GetIssue(issueId);
            if (!WasSuccessful(getIssueResponse))
            {
                log.ErrorWithStack("E-26933", $"Could not get issue date for issue id {issueId}. Error: {getIssueResponse.Data?.FormattedErrorMsg ?? getIssueResponse.ErrorMessage}");
                return null;
            }

            return getIssueResponse.Data;
        }

        // https://docs.atlassian.com/software/jira/docs/api/REST/7.6.1/#api/2/issue-addComment
        public bool AddComment(string issueId, string comment)
        {
            IRestResponse<BaseModel> addCommentResponse = this.client.AddComment(issueId, comment);
            if (!WasSuccessful(addCommentResponse))
            {
                log.ErrorWithStack("E-26934", $"Could not add comment to issue id {issueId}. Error: {addCommentResponse.Data?.FormattedErrorMsg ?? addCommentResponse.ErrorMessage}");
                return false;
            }

            return true;
        }
    }
}
