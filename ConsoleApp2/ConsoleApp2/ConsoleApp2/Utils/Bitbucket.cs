﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using AlarmBitbucketListener.Utils.Models;
using AlarmUtils;

namespace AlarmBitbucketListener.Utils
{
    class Bitbucket
    {
        private static readonly Logger Log = Logger.GetInstance();
        public static string Bitbcucket_BaseURL = ConfigurationHelper.GetAppSettings("BitbucketURL");        
        public string project_key = "FALCOR";
        public string repo_slug = "software";
        public string[] list_of_push_directories = { "Websites/Web/customer-ember/", "Websites/Web/CustomerDotNet/WebApi" };

        public WebRequestUtils webRequestUtils;
        public Uri bburl;
        public string username;
        public string password;
        public Bitbucket()
        {
            this.username = Environment.GetEnvironmentVariable("ATL_USER");
            this.password = Environment.GetEnvironmentVariable("ATL_PASSWORD");
            this.bburl = new Uri(Bitbcucket_BaseURL + "/rest/api/latest/");
            this.webRequestUtils = new WebRequestUtils(bburl, username, password);            
        }

        #region Helpers
  

        public static bool IsSuccessful(IRestResponse response)
        {
            return (int)response.StatusCode >= 200 && (int)response.StatusCode <= 299 && response.ResponseStatus == ResponseStatus.Completed;
        }

        #endregion Helpers


        public IRestResponse GetPullRequestChanges(int pull_request_number)
        {
            string queryUrl = "projects/" + project_key + "/repos/" + repo_slug + "/pull-requests/" + pull_request_number + "/changes";
            IRestResponse restResponse = webRequestUtils.GetRequest(queryUrl);
            Log.Info("Getting Pull Request changes for " + pull_request_number+ ":"+ restResponse.Content);
            if (!IsSuccessful(restResponse))
            {
               Log.Error("E-2228","Could not get pull request changes date for pull_request_number id {pull_request_number}. Error: {restResponse.Data?.FormattedErrorMsg ?? restResponse.ErrorMessage}");
                return null;
            }
            return restResponse;

        }

        public bool DoesRequirePushBuild(int pull_request_number)
        {
            IRestResponse restResponse = GetPullRequestChanges(pull_request_number);
            PullRequestChangesFields.RootObject pullRequestChangesFields = webRequestUtils.deserializer.Deserialize<PullRequestChangesFields.RootObject>(restResponse);

            Log.Info("Got Pull Request changes for pull request "+ pull_request_number);
            // Check if any of the changed file are in list_of_push_directories. If yes, then addition push build is required
            int totalMatchCount = 0;
            foreach (var value in pullRequestChangesFields.values)
            {
                string parentdir = value.path.parent;
                Log.Info("Got Pull Request changes for pull request " + pull_request_number+" and parent dir is" + parentdir);
                if (parentdir != "")
                {
                    var result2 = list_of_push_directories.Where(item => item.StartsWith(parentdir));
                    totalMatchCount = totalMatchCount + getICount(result2);
                }                
            }
            Log.Info("Got Pull Request changes for pull request " + pull_request_number + " and totalMatchCount  is" + totalMatchCount);
            if (totalMatchCount != 0)
            {
                return true;
            }
            else
            {
                return false;
            }            
        }

        public int getICount(IEnumerable<string> source )
        {
            int result = 0;
            using (IEnumerator<string> enumerator = source.GetEnumerator())
            {
                while (enumerator.MoveNext())
                    result++;
            }
            return result;
        }
    }
}
