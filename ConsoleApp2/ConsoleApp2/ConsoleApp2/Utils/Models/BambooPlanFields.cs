﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmBitbucketListener.Utils.Models
{
    class BambooPlanFields
    {
        public class Link
        {
            public string href { get; set; }
            public string rel { get; set; }
        }

        public class Project
        {
            public string key { get; set; }
            public string name { get; set; }
            public string description { get; set; }
            public Link link { get; set; }
        }

        public class Master
        {
        }

        public class Link2
        {
            public string href { get; set; }
            public string rel { get; set; }
        }
                                    
        public class PlanKey
        {
            public string key { get; set; }
        }

        public class RootObject
        {
            public string expand { get; set; }
            public string projectKey { get; set; }
            public string projectName { get; set; }
            public Project project { get; set; }
            public Master master { get; set; }
            public string shortName { get; set; }
            public string buildName { get; set; }
            public string shortKey { get; set; }
            public string type { get; set; }
            public bool enabled { get; set; }
            public Link2 link { get; set; }
            public bool isFavourite { get; set; }
            public bool isActive { get; set; }
            public bool isBuilding { get; set; }
            public int averageBuildTimeInSeconds { get; set; }    
            public string key { get; set; }
            public string name { get; set; }
            public PlanKey planKey { get; set; }
        }
    }
}
