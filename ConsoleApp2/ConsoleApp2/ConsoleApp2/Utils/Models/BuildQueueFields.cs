﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmBitbucketListener.Utils.Models
{
    class BuildQueueFields
    {
        public class Link
        {
            public string href { get; set; }
            public string rel { get; set; }
        }

        public class RootObject
        {
            public string planKey { get; set; }
            public int buildNumber { get; set; }
            public string buildResultKey { get; set; }
            public string triggerReason { get; set; }
            public Link link { get; set; }
        }
    }
}
