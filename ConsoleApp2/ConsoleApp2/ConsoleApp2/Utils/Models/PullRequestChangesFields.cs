﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmBitbucketListener.Utils.Models
{
    class PullRequestChangesFields
    {
        public class Properties
        {
            public string changeScope { get; set; }
        }

        public class Path
        {
            public List<string> components { get; set; }
            public string parent { get; set; }
            public string name { get; set; }
            public string extension { get; set; }
            public string toString { get; set; }
        }

        public class Links
        {
            public List<object> self { get; set; }
        }

        public class Properties2
        {
            public string gitChangeType { get; set; }
        }

        public class Value
        {
            public string contentId { get; set; }
            public string fromContentId { get; set; }
            public Path path { get; set; }
            public bool executable { get; set; }
            public int percentUnchanged { get; set; }
            public string type { get; set; }
            public string nodeType { get; set; }
            public bool srcExecutable { get; set; }
            public Links links { get; set; }
            public Properties2 properties { get; set; }
        }

        public class RootObject
        {
            public string fromHash { get; set; }
            public string toHash { get; set; }
            public Properties properties { get; set; }
            public List<Value> values { get; set; }
            public int size { get; set; }
            public bool isLastPage { get; set; }
            public int start { get; set; }
            public int limit { get; set; }
            public object nextPageStart { get; set; }
        }
    }
}
