﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlarmBitbucketListener.Utils.Models
{
    class BambooPlanBranchFields
    {
        public class Link
        {
            public string href { get; set; }
            public string rel { get; set; }
        }

        public class Link2
        {
            public string href { get; set; }
            public string rel { get; set; }
        }

        public class PlanKey
        {
            public string key { get; set; }
        }

        public class Master
        {
            public string shortName { get; set; }
            public string shortKey { get; set; }
            public string type { get; set; }
            public bool enabled { get; set; }
            public Link2 link { get; set; }
            public string key { get; set; }
            public string name { get; set; }
            public PlanKey planKey { get; set; }
        }

        public class RootObject
        {
            public string expand { get; set; }
            public string description { get; set; }
            public string shortName { get; set; }
            public string shortKey { get; set; }
            public bool enabled { get; set; }
            public bool isFavourite { get; set; }
            public Link link { get; set; }
            public Master master { get; set; }
            public string workflowType { get; set; }
            public string key { get; set; }
            public string name { get; set; }
        }
    }
}
