﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlarmBitbucketListener.Utils.Models;
using AlarmUtils;

namespace AlarmBitbucketListener.Utils
{
    class Bamboo
    {
        private static readonly Logger Log = Logger.GetInstance();

        public static string Bamboo_BaseURL = ConfigurationHelper.GetAppSettings("BambooURL");
        public string Generic_Build_Plan_Key = ConfigurationHelper.GetAppSettings("Generic_Build_Plan_Key");
        public string Ember_Build_Push_Key = ConfigurationHelper.GetAppSettings("WEB_Build_Push_Key");

        public WebRequestUtils webRequestUtils;
        public Uri bamurl;
        public string username;
        public string password;
                 
        
        public Bamboo()
        {
            this.username = Environment.GetEnvironmentVariable("ATL_USER");
            this.password = Environment.GetEnvironmentVariable("ATL_PASSWORD");
            this.bamurl = new Uri(Bamboo_BaseURL + "/rest/api/latest/");
            this.webRequestUtils = new WebRequestUtils(bamurl, username, password);
        }

        #region Helpers


        public static bool IsSuccessful(IRestResponse response)
        {
            return (int)response.StatusCode >= 200 && (int)response.StatusCode <= 299 && response.ResponseStatus == ResponseStatus.Completed;
        }

        #endregion Helpers

        public IRestResponse Get_existing_plan_details(string branch_name, string bamboo_plan_key)
        {
            IRestResponse restResponse = webRequestUtils.GetRequest("/plan/" + bamboo_plan_key + "/branch/" + branch_name + ".json");
            if (!IsSuccessful(restResponse))
            {
                Log.Error("E-2229", "Could not get get existing plan details  for branch " +branch_name+ "for bamboo plan"+ bamboo_plan_key +". Error: {restResponse.Data?.FormattedErrorMsg ?? restResponse.ErrorMessage}");
                return null;
            }
            return restResponse;
        }

        public string Get_plan_branch_key(IRestResponse restResponse)
        {            
            if (restResponse.Content == "")
            {
                return null;
            }
            BambooPlanBranchFields.RootObject bambooPlanBranchFields = webRequestUtils.deserializer.Deserialize<BambooPlanBranchFields.RootObject>(restResponse);
            return bambooPlanBranchFields.key;
        }

        public IRestResponse Get_runningstate_planbranch(string existingPlanBranchKey)
        {
            IRestResponse restResponse = webRequestUtils.GetRequest("/plan/" + existingPlanBranchKey + ".json");
            if (!IsSuccessful(restResponse))
            {
                Log.Error("E-2229", "Could not get Get_runningstate_planbranch existing plan details  for branch " + existingPlanBranchKey + ". Error: {restResponse.Data?.FormattedErrorMsg ?? restResponse.ErrorMessage}");
                return null;
            }
            return restResponse;  
        }

        public bool Get_runningstate(IRestResponse restResponse)
        {
            if (restResponse.Content == "")
            {
                return false;
            }
            BambooPlanFields.RootObject bambooPlanFields = webRequestUtils.deserializer.Deserialize<BambooPlanFields.RootObject>(restResponse);
            return bambooPlanFields.isActive;
        }

        public IRestResponse Create_plan_branch(string branch_name, string bamboo_plan_key )
        {	
            IRestResponse restResponse = webRequestUtils.PutRequest("/plan/" + bamboo_plan_key +"/branch/" + branch_name + ".json?enabled=False&cleanupEnabled=True");
            if (!IsSuccessful(restResponse))
            {
                Log.Error("E-3229", "Could not get Create_plan_branch existing plan details  for branch " + branch_name + "bamboo plan key"+ bamboo_plan_key+". Error: {restResponse.Data?.FormattedErrorMsg ?? restResponse.ErrorMessage}");
                return null;
            }
            return restResponse;
        }

        public IRestResponse Enable_plan_branch( string planbranchkey )
        {
            IRestResponse restResponse = webRequestUtils.PostRequest("/plan/" + planbranchkey + "/enable");
            if (!IsSuccessful(restResponse))
            {
                Log.Error("E-3209", "Could not get Enable_plan_branch existing plan details  for branch " + planbranchkey + ". Error: {restResponse.Data?.FormattedErrorMsg ?? restResponse.ErrorMessage}");
                return null;
            }
            return restResponse;
        }

        public IRestResponse  Queue_planbranch( string existingPlanBranchKey )
        {
            IRestResponse restResponse = webRequestUtils.PostRequest("/queue/" + existingPlanBranchKey + ".json");
            if (!IsSuccessful(restResponse))
            {
                Log.Error("E-3219", "Could not get Queue_planbranch existing plan details  for branch " + existingPlanBranchKey + ". Error: {restResponse.Data?.FormattedErrorMsg ?? restResponse.ErrorMessage}");
                return null;
            }
            return restResponse;
        }

        public string Get_queuedPlan_key(IRestResponse restResponse)
        {
            if (restResponse.Content == "")
            {
                return null;
            }
            BuildQueueFields.RootObject buildQueueFields = webRequestUtils.deserializer.Deserialize<BuildQueueFields.RootObject>(restResponse);
            return buildQueueFields.buildResultKey;
        }

        public string Trigger_build( string branch_name, string bamboo_plan_key, int main_row_id )
        {            
            string existingPlanBranchKey = Get_plan_branch_key(Get_existing_plan_details(branch_name, bamboo_plan_key));
            
            if ( existingPlanBranchKey == null ){
                Log.Info("No branch found for  plan . Need to create new one" );		      
		        existingPlanBranchKey = Get_plan_branch_key(Create_plan_branch(branch_name, bamboo_plan_key));
            }
            
            Log.Info("Enable plan : " + existingPlanBranchKey);
            Enable_plan_branch(existingPlanBranchKey);

            if ( bamboo_plan_key.Equals("Ember_Build_Push_Key") ){
                Log.Info("Build plan doesn't need to be triggered. It should be run manually. ");
                return existingPlanBranchKey;
            }    

            if ( ! ( Get_runningstate(Get_runningstate_planbranch(existingPlanBranchKey)) ) ) {
                string build_result_key = Get_queuedPlan_key(Queue_planbranch(existingPlanBranchKey));
                if ( build_result_key == null){
                    Log.Info("New plan couldnt be triggered with value . try again!.");      
                }
                Log.Info("New plan triggered with value build_result_key. Insert it in Database.");
                return build_result_key;
            }
            return ""; 
            
        }
    }
}
