﻿using System;
using System.Net;
using RestSharp;
using RestSharp.Authenticators;
using RestSharp.Serialization.Json;
using AlarmBitbucketListener.Utils.Models;
using System.Linq;

namespace AlarmBitbucketListener.Utils
{
    class WebRequestUtils : RestClient
    {
        public RestClient client;
        public JsonDeserializer deserializer;
       
        public WebRequestUtils(Uri BaseUrl, string username, string password)
        {            
            this.BaseUrl = BaseUrl;            
            this.Authenticator = new HttpBasicAuthenticator(username, password);
            deserializer = new JsonDeserializer();

            client = new RestClient(BaseUrl)
            {
                Authenticator = Authenticator
            };
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.DefaultConnectionLimit = 9999;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12 | SecurityProtocolType.Ssl3;
            ServicePointManager.ServerCertificateValidationCallback +=
        (sender, certificate, chain, sslPolicyErrors) => true;

        }

        #region Helpers

        private IRestResponse MakeCall(IRestRequest request)
        {
            try
            {
                return this.Execute(request);
            }
            catch (Exception ex)
            {
                return new RestResponse { ResponseStatus = ResponseStatus.Aborted, ErrorException = ex, ErrorMessage = ex.Message };
            }
        }
           
        #endregion Helpers

        public IRestResponse GetRequest(string queryUrl)
        {                       
            RestRequest request = new RestRequest(queryUrl, Method.GET);            
            request.AddHeader("Accept", "application/json");                
            return this.MakeCall(request);
        }



        public IRestResponse PostRequest(string queryUrl)
        {
            RestRequest request = new RestRequest(queryUrl, Method.POST);
            request.AddHeader("Accept", "application/json");            
            return this.MakeCall(request);
        }

        public IRestResponse PostRequest(string queryUrl,string body)
        {
            RestRequest request = new RestRequest(queryUrl, Method.POST);
            request.AddHeader("Accept", "application/json");
            request.AddJsonBody(body);         
            return this.MakeCall(request);
        }

        public IRestResponse PutRequest(string queryUrl)
        {
            RestRequest request = new RestRequest(queryUrl, Method.PUT);
            request.AddHeader("Accept", "application/json");           
            return this.MakeCall(request);
        }


    }




}
