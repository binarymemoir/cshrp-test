﻿using AlarmUtils;
using System;
using System.Data;
using AlarmBitbucketListener.Utils;
using BackendUtils;

namespace AlarmBitbucketListener
{
    public class BitbucketListener
    {
        private static readonly Logger Log = Logger.GetInstance();

        private int MaxQueueCount = 1;
        private long MaxConcurrentRuns = 1;

        public void DoInitialize()
        {
            MaxConcurrentRuns = ConfigurationHelper.GetAppSettings("MaxConcurrentRuns",1);
            MaxQueueCount = ConfigurationHelper.GetAppSettings("MaxQueueCount",1);
        }

        public void DoLogSettings()
        {         
            Log.Info($"MaxConcurrentRuns={MaxConcurrentRuns}");
            Log.Info($"MaxQueueCount={MaxQueueCount}");
        }

        public WorkResultEnum DoPrimaryWork()
        {
            // INF-5678: the true meaning of "WorkResultEnum.ResultFatalError" is a catastrophique failure, not that a command failed to start.
            try
            {
                StartAndStopExecutions();
                //ArchiveAutomatedTestOutboxCommands();
                //AuditContinuousTests();
            }
            catch (Exception ee)
            {
                Log.Error("E-28267", $"Exception in DoPrimaryWork()", ee);
                return WorkResultEnum.ResultFatalError;
            }

            return WorkResultEnum.ResultSuccess;
        }

        public void StartAndStopExecutions()
        {                 
            Runtask();
        }

        public static string GetPreviousRecordedTimeStamp()
        {
            string sqlQuery = "Select top 1 [record_time] from listener_pull_requests order by row_id DESC";
            DataSet lDataSet = DBHelper.GetDataset(sqlQuery);

            DataRow row1 = lDataSet.Tables[0].Rows[0];
            foreach (DataRow row in lDataSet.Tables[0].Rows)
            {
                DateTime record_time = (DateTime)row["record_time"];
                return record_time.ToString("MM/dd/yyyy HH:mm:ss");

            }
            return DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
        }

        public static void Runtask()
        {
            string lastRecorded_timestamp = GetPreviousRecordedTimeStamp();
            Log.Info(msg: "got time  " + lastRecorded_timestamp);

            Log.Info(msg: "get open PRs from Bitbucket");
            string sqlQuery = "exec spa_listener_get_pullrequests_from_bitbucket " + "'" + lastRecorded_timestamp + "'" + " ," + 1;

            Log.Info("running query:" + sqlQuery);
            DBHelper.GetDataset(sqlQuery);

            Log.Info("Mark all deleted PRs with tracking_id =3");
            string abandonSQLquery = "exec spa_listener_abandon_deleted_pullrequests";
            DBHelper.GetDataset(abandonSQLquery);

            Log.Info("Mark all PRs with new commits with tracking_id =2");
            string abandonOldCommitsSQLquery = "exec spa_listener_abandon_oldcommits";
            DBHelper.GetDataset(abandonOldCommitsSQLquery);

            Log.Info("Now you are ready to get open PRs from DB");
            string getOpenPRFromMainDBSQL = "exec spa_listener_get_pullrequests_pending";
            DataSet openPrs = DBHelper.GetDataset(getOpenPRFromMainDBSQL);

            if (DBHelper.DatasetIsEmpty(openPrs))
            {
                Log.Info("No open Pull request found");
            }

            DataRowCollection rows = openPrs.Tables[0].Rows;            
            Log.Info("Number of Open PRs is " + rows.Count);

            Bitbucket bitbucket = new Bitbucket();
            Bamboo bamboo = new Bamboo();

            foreach ( DataRow pr in rows)
            {
                string prefix = "PR: " + pr["pull_request_number"] + " :Branch :" + pr["branch_name"] + " :Repository: " + pr["repository"] + " :Rowid " + pr["row_id"];
                int pull_request_number = (int) (pr["pull_request_number"]) ;
                string branch_name = (string) (pr["branch_name"]);
                int row_id = Convert.ToInt32(pr["row_id"]);
                int repository_id = Convert.ToInt32(pr["repository"]);
                if (repository_id == 144)
                {
                    bool doesRequirePushBuild = bitbucket.DoesRequirePushBuild(pull_request_number);
                    if (doesRequirePushBuild)
                    {
                        Log.Info(prefix + ": Requires Web Push plan ");
                        bamboo.Trigger_build(branch_name, bamboo.Ember_Build_Push_Key, row_id);
                    }
                }               

                bamboo.Trigger_build(branch_name, bamboo.Generic_Build_Plan_Key, row_id);
            }         
           

        }

    }
}