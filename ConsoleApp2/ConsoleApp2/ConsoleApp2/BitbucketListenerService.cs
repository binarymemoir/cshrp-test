﻿using AlarmUtils;
using BackendUtils;
using System;

namespace AlarmBitbucketListener
{
    public partial class BitbucketListenerService : BaseBackendService
    {
        #region Variables

        private static readonly Logger Log = Logger.GetInstance();

        private static readonly BitbucketListener bitbucketListener = new BitbucketListener();

        private const string APPLICATION_NAME = "BitbucketListenerService";
        private const int APPLICATION_TYPE = 105066;

        #endregion

        #region Properties

        protected override string AppName => APPLICATION_NAME;

        protected override int AppType => APPLICATION_TYPE;

        #endregion

        public BitbucketListenerService()
        {
            InitializeComponent();
        }

        protected override void DoInitialize()
        {
            bitbucketListener.DoInitialize();
        }

        protected override void DoLogSettings()
        {
            bitbucketListener.DoLogSettings();
        }

        protected override WorkResultEnum DoPrimaryWork()
        {
            return bitbucketListener.DoPrimaryWork();
        }

        protected override WorkResultEnum DoBackupWork(int rank)
        {
            try
            {
                bitbucketListener.StartAndStopExecutions();
            }
            catch (Exception ee)
            {
                Log.Error("E-28271", $"Exception in DoBackupWork()", ee);
                return WorkResultEnum.ResultFatalError;
            }

            return WorkResultEnum.ResultSuccess;
        }
    }
}