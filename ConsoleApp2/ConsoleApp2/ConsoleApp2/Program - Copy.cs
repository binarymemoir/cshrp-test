﻿using AlarmUtils;
using System.ServiceProcess;

namespace try3toloadAlarmUtils
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            if (!args.IsNullOrEmpty())
            {
                ManualRun(args);
            }
            else
            {
                ServiceBase[] servicesToRun = { new BitbucketListenerService() };
                ServiceBase.Run(servicesToRun);
            }
        }
        
        private static void ManualRun(string[] args)
        {
            BitbucketListener bitbucketListener = new BitbucketListener();
            bitbucketListener.DoInitialize();
            bitbucketListener.DoLogSettings();

            while (true)
            {
                bitbucketListener.DoPrimaryWork();
                System.Threading.Thread.Sleep(30000);
            }
        }
    }
}