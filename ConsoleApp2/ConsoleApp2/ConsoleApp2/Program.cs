﻿using AlarmUtils;
using System.ServiceProcess;

namespace AlarmBitbucketListener
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            ManualRun(args);
        }
        
        private static void ManualRun(string[] args)
        {
            BitbucketListener bitbucketListener = new BitbucketListener();
            bitbucketListener.DoInitialize();
            bitbucketListener.DoLogSettings();

            while (true)
            {
                bitbucketListener.DoPrimaryWork();
                System.Threading.Thread.Sleep(30000);
            }
        }
    }
}