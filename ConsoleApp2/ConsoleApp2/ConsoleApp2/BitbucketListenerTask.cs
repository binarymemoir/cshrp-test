﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AlarmUtils;

namespace try3toloadAlarmUtils

{
    class BitbucketListenerTask
    {
        private static readonly Logger logger = Logger.GetInstance();
        public static string GetPreviousRecordedTimeStamp(DateTime starttime)
        {
            string sqlQuery = "Select top 1 [record_time] from listener_pull_requests order by row_id DESC";
            DataSet lDataSet = DBHelper.GetDataset(sqlQuery);
            logger.Info("get timestap from dbhelper");

            foreach (DataRow row in lDataSet.Tables[0].Rows)
            {
                DateTime record_time = (DateTime)row["record_time"];
                return record_time.ToString("MM/dd/yyyy HH:mm:ss");

            }
            return starttime.ToString("MM/dd/yyyy HH:mm:ss");
        }

        public static string runtask(DateTime starttime)
        {
            string lastRecorded_timestamp = GetPreviousRecordedTimeStamp(starttime);
            logger.Info(msg: "got time  " + lastRecorded_timestamp);

            logger.Info(msg: "get open PRs from Bitbucket");
            string sqlQuery = "exec spa_listener_get_pullrequests_from_bitbucket " + "'" + lastRecorded_timestamp + "'" + " ," + 1;

            logger.Info("running query:"+ sqlQuery);
            DBHelper.GetDataset(sqlQuery);

            logger.Info("Mark all deleted PRs with tracking_id =3");
            string abandonSQLquery = "exec spa_listener_abandon_deleted_pullrequests";
            DBHelper.GetDataset(abandonSQLquery);

            logger.Info("Mark all PRs with new commits with tracking_id =2");
            string abandonOldCommitsSQLquery = "exec spa_listener_abandon_oldcommits";
            DBHelper.GetDataset(abandonOldCommitsSQLquery);

            logger.Info("Now you are ready to get open PRs from DB");
            string getOpenPRFromMainDBSQL = "exec spa_listener_get_pullrequests_pending";
            DataSet openPrs = DBHelper.GetDataset(getOpenPRFromMainDBSQL);

            if (DBHelper.DatasetIsEmpty(openPrs))
            {
                logger.Info("No open Pull request found");
            }

            DataRowCollection rows = openPrs.Tables[0].Rows;
            int count = rows.Count;
            logger.Info("Number of Open PRs is {0}"+ count);

            //int tracking_id = 5;
            foreach (DataRow pr in rows)
            {
                string prefix = "PR: " + pr["pull_request_number"] + " :Branch :" + pr["branch_name"] + " :Repository: " + pr["repository"] + " :Rowid " + pr["row_id"];

                logger.Info("see what did you get "+ prefix);
                
            }


            return "hi";

        }


    }
}
